### How do I get set up? ###

Tutorial to get started:
http://guides.rubyonrails.org/getting_started.html

## Setup: ##
1. Install Ruby using RubyInstaller, then download the DevKit according to your ruby version.
http://rubyinstaller.org/downloads/

2. Instruction on how to install DevKit:
https://github.com/oneclick/rubyinstaller/wiki/development-kit

3. Follow the tutorial in the link above. For first time, do a "bundle install" to install all dependencies specified in the Gemfile.lock.
Change from using the byebug to pry for debugging. To put a breakpoint "binding.pry"